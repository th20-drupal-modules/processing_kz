<?php

/**
 * @file
 * Admin page callbacks and forms.
 */

/**
 * Form builder. Module settings form.
 */
function processing_kz_configure_form($form, &$form_state) {
  $form['processing_kz_merchant_id'] = array(
    '#type' => 'textfield',
    '#title' => t('Merchant ID'),
    '#required' => TRUE,
    '#default_value' => variable_get('processing_kz_merchant_id'),
    '#maxlength' => 15,
  );

  $form['processing_kz_debug_mode'] = array(
    '#type' => 'checkbox',
    '#title' => t('Processing.kz is in the debug mode?'),
    '#description' => t('If the debug mode is enabled, payments will be processed by the test gateway.'),
    '#default_value' => variable_get('processing_kz_debug_mode'),
  );

  $form['processing_kz_url_wsdl'] = array(
    '#type' => 'textfield',
    '#title' => t('WSDL resource URL'),
    '#description' => t('You should use the default value unless you are instructed to change it.'),
    '#required' => TRUE,
    '#default_value' => variable_get('processing_kz_url_wsdl', 'https://payment.processinggmbh.ch/CNPMerchantWebServices/services/CNPMerchantWebService?wsdl'),
    '#maxlength' => 120,
  );

  $form['processing_kz_url_service'] = array(
    '#type' => 'textfield',
    '#title' => t('Payment service URL'),
    '#description' => t('You should use the default value unless you are instructed to change it.'),
    '#required' => TRUE,
    '#default_value' => variable_get('processing_kz_url_service', 'https://payment.processinggmbh.ch/CNPMerchantWebServices/services/CNPMerchantWebService'),
    '#maxlength' => 120,
  );

  return system_settings_form($form);
}

/**
 * Menu page callback.
 */
function processing_kz_transactions_overview_page() {
  $page = array();

  $transactions = db_select('processing_kz_transactions', 't')
    ->extend('PagerDefault')
    ->fields('t')
    ->orderBy('id', 'DESC')
    ->execute()
    ->fetchAll();

  $header = array(
    t('Transaction ID'),
    t('Order ID'),
    t('Amount'),
    t('Date and time'),
    t('Reference'),
    t('Authorization code'),
    t('Status'),
    '',
  );

  $rows = array();
  foreach ($transactions as $transaction) {
    $row = array(
      'data' => array(),
      'class' => array(),
    );
    $actions = array();

    if ($transaction->processed && !$transaction->completed) {
      $actions[] = l(
        t('Complete'),
        sprintf('admin/config/services/processing_kz/transactions/%s/%s/complete',
          $transaction->id,
          $transaction->token
        ),
        array('query' => array('destination' => $_GET['q']))
      );
      $actions[] = l(
        t('Cancel'),
        sprintf('admin/config/services/processing_kz/transactions/%s/%s/cancel',
          $transaction->id,
          $transaction->token
        ),
        array('query' => array('destination' => $_GET['q']))
      );
    }

    $row['data'][] = check_plain($transaction->id);
    $row['data'][] = check_plain($transaction->order_id);
    $row['data'][] = format_string('@a @c', array(
      '@a' => $transaction->amount / 100,
      '@c' => check_plain($transaction->currency_code),
    ));
    $row['data'][] = format_date($transaction->created, 'short');
    $row['data'][] = check_plain($transaction->reference);
    $row['data'][] = check_plain($transaction->auth_code);
    $row['data'][] = check_plain($transaction->status);

    $row['data'][] = theme('item_list', array(
      'attributes' => array('class' => array('inline')),
      'items' => $actions,
    ));

    $row['class'][] = 'transaction-status-' . check_plain(strtolower($transaction->status));
    if ($transaction->completed) {
      $row['class'][] = 'transaction-completed';
    }
    if (!$transaction->processed) {
      $row['class'][] = 'transaction-unprocessed';
    }

    $rows[] = $row;
  }

  if (empty($rows)) {
    $rows[] = array(
      array(
        'colspan' => 7,
        'data' => t('<i>No transactions at the moment.</i>'),
      ),
    );
  }

  $page['table'] = array(
    '#theme' => 'table',
    '#header' => $header,
    '#rows' => $rows,
    '#attributes' => array(
      'class' => array('transactions-overview-table'),
    ),
  );

  $page['pager'] = array(
    '#theme' => 'pager',
  );
  return $page;
}

/**
 * Form building callback.
 */
function processing_kz_transaction_complete_form($form, &$form_state, $transaction) {
  $return = 'admin/config/services/processing_kz/transactions';

  $form['#transaction'] = $transaction;
  $form['#validate'] = array('processing_kz_transaction_complete_cancel_form_validate');
  $form['#submit'] = array('processing_kz_transaction_complete_form_submit');

  return confirm_form(
    $form,
    t('Are you sure that you want to complete this transaction manually?'),
    $return,
    t('Transactions are managed automatically when you change order status. You should perform this action manually only in rare cases when you encounter technical difficulties.'),
    t('Complete transaction')
  );
}

/**
 * Form building callback.
 */
function processing_kz_transaction_cancel_form($form, &$form_state, $transaction) {
  $return = 'admin/config/services/processing_kz/transactions';

  $form['#transaction'] = $transaction;
  $form['#validate'] = array('processing_kz_transaction_complete_cancel_form_validate');
  $form['#submit'] = array('processing_kz_transaction_cancel_form_submit');

  return confirm_form(
    $form,
    t('Are you sure that you want to cancel this transaction manually?'),
    $return,
    t('Transactions are managed automatically when you change order status. You should perform this action manually only in rare cases when you encounter technical difficulties.'),
    t('Cancel transaction')
  );
}

/**
 * Form validate handler.
 */
function processing_kz_transaction_complete_cancel_form_validate($form, &$form_state) {
  $transaction = $form['#transaction'];

  if (!$transaction->processed || $transaction->completed) {
    form_set_error('', t('You cannot change this transaction status manually at the moment.'));
  }
}

/**
 * Form submit handler.
 */
function processing_kz_transaction_complete_form_submit($form, &$form_state) {
  libraries_load('processing_kz');

  $transaction = $form['#transaction'];
  processing_kz_complete_transaction($transaction);

  drupal_set_message(t('Transaction has been completed.'));
}

/**
 * Form submit handler.
 */
function processing_kz_transaction_cancel_form_submit($form, &$form_state) {
  libraries_load('processing_kz');

  $transaction = $form['#transaction'];
  processing_kz_cancel_transaction($transaction);

  drupal_set_message(t('Transaction has been canceled.'));
}
