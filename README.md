Processing.kz payment gateway for Drupal 7
==========================================

Add processing.kz payment option to your Drupal 7 e-commerce website. This
module has rather a limited regional usage. For more information,
[check the processing.kz homepage][2].

To use this module you need to have a valid account in the processing.kz system.

Module includes an implementation of a payment method for Drupal Commerce
e-commerce solution.


Платёжный шлюз processing.kz для Drupal 7
=========================================

Реализация способа оплаты через платёжный шлюз processing.kz для
интернет-магазинов на основе Drupal 7. Подробную информацию о processing.kz вы
можете найти [на официальном сайте][1].

Для использования processing.kz в качестве способа оплаты, вы должны заключить
договор с провайдером услуги и иметь действующую учётную запись. Подробную
информацию вы можете найти на официальном сайте processing.kz.

Инструкцию по установке и использованию вы можете найти в файле [INSTALL.md][3].

Для интеграции с платёжным шлюзом, модуль использует возможности
[SOAP-клиента для API][4].

Модуль включает реализацию метода оплаты для Drupal Commerce.


Warning / Внимание / Fais attention / 注意
------------------------------------------

<blockquote>
  This module will allow payments only if the order currency is KZT. Payment
  will not be available if you use USD, EUR or any other currency on your
  e-commerce site.
</blockquote>

<blockquote>
  Платеж будет разрешён только в том случае, если в качестве валюты заказа
  указано KZT. Пользователь не будет допущен до платежа, если в вашем магазине
  вы используете USD, EUR или любую другую валюту.
</blockquote>


[1]: http://processing.kz/
[2]: http://processing.kz/en
[3]: INSTALL.md
[4]: https://github.com/iborodikhin/processing-kz
