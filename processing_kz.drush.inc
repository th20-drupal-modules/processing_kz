<?php

/**
 * @file
 * Drush integration for the processing_kz module.
 */

/**
 * Implements hook_drush_command().
 */
function processing_kz_drush_command() {
  $items['processingkz-download'] = array(
    'callback' => 'processing_kz_drush_download',
    'description' => dt('Downloads the latest version of the API client from Github.'),
    'arguments' => array(),
  );
  return $items;
}

/**
 * Downloads the API client source and installs into default libraries path.
 */
function processing_kz_drush_download() {
  $library = libraries_load('processing_kz');
  if ($library['library path']) {
    drush_log(dt('Proccesing.kz API client is already downloaded.'), 'success');
    return;
  }
  if (empty($library['download url'])) {
    drush_log(dt('Client download URL is not specified. Please check your module installation and report this issue to the module maintainer.'), 'error');
    return;
  }

  $url = $library['download url'];
  $path = drush_get_context('DRUSH_DRUPAL_ROOT') . '/sites/all/libraries/processing_kz';

  if (is_file("$path/src/ProcessingKz/Client.php")) {
    drush_log(dt("Processing.kz API client appears to be already downloaded. Clear cache to refresh libraries status."), 'warning');
    return;
  }

  drush_log(dt('Downloading API client from @url', array('@url' => $url)));
  if ($destination = drush_download_file($url)) {
    $tmpPath = drush_tempdir();
    drush_tarball_extract($destination, $tmpPath);

    drush_copy_dir("$tmpPath/processing-kz-master", $path);
    drush_log(dt('Download completed. Clear cache to refresh libraries status.'), 'success');
  }
  else {
    drush_log(dt('Client download failed. Please check your Internet connection and try again.'), 'error');
  }
}

/**
 * Implements drush_MODULE_post_COMMAND().
 */
function drush_processing_kz_post_enable() {
  $modules = func_get_args();
  if (in_array('processing_kz', $modules) && !drush_get_option('skip')) {
    processing_kz_drush_download();
  }
}
