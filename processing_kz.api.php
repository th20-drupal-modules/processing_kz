<?php

/**
 * @file
 * Module API usage examples.
 */

/**
 * Implements hook_processing_kz_complete().
 *
 * Performs custom entity modifications and marks transaction completed.
 *
 * This hook is invoked only if transaction is not marked as completed and
 * payment was successful.
 *
 * @param object $transaction
 *   Transaction details object.
 *
 * @return boolean
 *   TRUE if successful transaction has been handled by the hook implementation;
 *   FALSE otherwise.
 */
function hook_processing_kz_process($transaction) {
  if ($transaction->module != 'example') {
    // This is a transaction initiated by some other module.
    return FALSE;
  }

  // Perform any custom actions.
  $order_id = $transaction->order_id;
  if ($order = example_order_load($order_id)) {
    example_order_add_payment($order, $transaction->amount / 100);
    example_order_set_status($order, 'paid');
    return TRUE;
  }

  return FALSE;
}

/**
 * Implements hook_processing_kz_return().
 *
 * Displays sucess or failure messages and return redirect url.
 *
 * This hook will be called after both successful and failed payments.
 * Use processing_kz_transaction_is_successful() helper to check transaction
 * status.
 *
 * It very important not to change any data in this hook becayse it may be
 * called any number of time for the same transaction. If you need to change
 * the underlying order record, use the hook_processing_kz_complete() hook.
 *
 * This hook is for notifications only.
 *
 * @param object $transaction
 *   Transaction details object.
 *
 * @return NULL|string
 *   An URL where user must be redirected after returning to site after payment.
 */
function hook_processing_kz_return($transaction) {
  if ($transaction->module != 'example') {
    // This is a transaction initiated by some other module.
    return;
  }

  if (processing_kz_transaction_is_successful($transaction)) {
    drupal_add_message('Your payment was successful!');
  }
  else {
    drupal_add_message('Something went wrong with your payment…', 'error');
  }

  return url('my-custom-success-page/' . $transaction->order_id);
}
