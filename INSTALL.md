Installation
============

Before install check all the following dependencies:

  1. The [libraries][2] module;
  2. PHP `SOAP` extension;
  3. PHP `OpenSSL` extension;

Extract module into the `sites/all/modules` or `sites/<site>/modules` directory
and do the following:

  1. Enable this module on the `admin/modules` page;

  2. Download the [SOAP client for processing.kz API][1] with:
     <pre>drush processingkz-download</pre>

  3. If you do not use `drush`, download and install SOAP client manually.
     Some module distributions already include the correct version of the
     client, in which case move to the next step;

  4. Clear all caches to let the client get detected;

  5. Go to the `admin/config/services/processing_kz` page to configure
     the *merchant ID*.


If you need to install the `SOAP client` manually, it must be installed into
the **libraries/processing_kz** directory. Additional information can be
found in the [libraries documentation][3]. Check that file
`libraries/processing_kz/src/ProcessingKz/Client.php`
exists and is readable.



Установка и настройка
=====================

Перед установкой модуля необходимо проверить наличие следующих зависимостей:

  1. Модуль [libraries][2];
  2. Расширение PHP `SOAP`;
  3. Расширение PHP `OpenSSL`;

Распакуйте модуль в папку `sites/all/modules` или `sites/<site>/modules`
и произведите следующие действия:

  1. Активируйте модуль на странице `admin/modules`;

  2. Скачайте [SOAP-клиент для API][1] командой:
     <pre>drush processingkz-download</pre>

  3. Если вы не используете `drush`, скачайте и установите SOAP-клиент вручную.
     В зависимости от поставки, правильная версия SOAP-клиента может входить
     в состав архива модуля. В таком случае, перейдите к следующему шагу;

  4. Отчистите кэш, чтобы скачанный клиент был корректно найден;

  5. Перейдите на страницу `admin/config/services/processing_kz` для настройки
     *идентификационного номера ТСП*.


В случае ручной установки `SOAP-клиента для API`, он должен быть установлен в
каталог **libraries/processing_kz**. См. подробную информацию в
[документации к модулю libraries][3]. Проверьте, что файл
`libraries/processing_kz/src/ProcessingKz/Client.php`
существует и доступен для чтения.


[1]: https://github.com/iborodikhin/processing-kz
[2]: https://drupal.org/project/libraries
[3]: https://drupal.org/node/1440066
