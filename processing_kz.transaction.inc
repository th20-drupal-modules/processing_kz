<?php

/**
 * @file
 * Transaction handling API.
 */

/**
 * Saves info about transaction into DB.
 */
function processing_kz_transaction_save($details) {
  global $language, $user;

  $details = (object) $details;
  if (empty($details->module) || empty($details->order_id) || empty($details->amount)) {
    watchdog('processing_kz', 'Could not save transaction record to DB: missing one or more required fields. Dumped object: @object', array(
      '@object' => var_dump($details),
    ), WATCHDOG_ERROR);
    return FALSE;
  }

  $defaults = array(
    'created'       => REQUEST_TIME,
    'completed'     => 0,
    'currency_code' => PROCESSING_KZ_KZT,
    'language'      => $language->language,
    'mail'          => !empty($user->mail) ? $user->mail : '',
    'processed'     => 0,
    'uid'           => $user->uid,
  );

  // Merge defaults.
  foreach ($defaults as $key => $value) {
    if (empty($details->$key)) {
      $details->$key = $value;
    }
  }

  // Fix possibly invalid language code.
  $details->language = _processing_kz_preprocess_language_code($details->language);

  // Fix possibly invalid goods list.
  if (empty($details->goods) || !is_array($details->goods)) {
    $details->goods = array();
  }

  // Set security token for client validation.
  if (empty($details->token)) {
    $details->token = drupal_get_token('processing.kz transaction token ' . mt_rand());
  }

  if (empty($details->id)) {
    // Reset any preset values for new transactions.
    $details->reference = NULL;
    $details->status = NULL;

    $result = drupal_write_record('processing_kz_transactions', $details);
  }
  else {
    $result = drupal_write_record('processing_kz_transactions', $details, 'id');
  }

  return $result ? $details : FALSE;
}

/**
 * Checks that language code is one of the allowed values.
 *
 * @param string $language
 *   Language code for validation.
 *
 * @return string
 *   Language code valid for transaction.
 */
function _processing_kz_preprocess_language_code($language) {
  switch ($language) {
    case 'RU':
    case 'ru':
      return 'ru';

    case 'KZ':
    case 'kz':
    case 'KK':
    case 'kk':
      return 'kz';

    default:
      return 'en';
  }
}

/**
 * Starts payment transaction.
 *
 * @param array $details
 *   Transaction details in a format suitable for writing into
 *   'processing_kz_transactions' table.
 *
 * @return ProcessingKz\Objects\Entity\StartTransactionResult || FALSE
 *   Result of a remote procedure call, of FALSE in case of an error.
 */
function processing_kz_start_transaction(array $details) {
  $details = processing_kz_transaction_save($details);
  if (empty($details)) {
    return FALSE;
  }

  $return_url = url(
    sprintf('processingkz/return/%s/%s', $details->id, $details->token),
    array('absolute' => TRUE)
  );

  $detailsEntity = new \ProcessingKz\Objects\Entity\TransactionDetails();
  $detailsEntity
    ->setCurrencyCode($details->currency_code)
    ->setLanguageCode($details->language)
    ->setMerchantId(processing_kz_get_merchant_id())
    ->setMerchantLocalDateTime(date('d.m.Y H:i:s', $details->created))
    ->setOrderId($details->id)
    ->setPurchaserEmail($details->mail)
    ->setReturnURL($return_url)
    ->setTotalAmount($details->amount);

  foreach ($details->goods as $name => $amount) {
    $item = new \ProcessingKz\Objects\Entity\GoodsItem();
    $item
      ->setNameOfGoods(trim($name))
      ->setAmount($amount)
      ->setCurrencyCode($detailsEntity->getCurrencyCode());

    $detailsEntity->setGoodsList($item);
  }

  $client = processing_kz_create_client();
  $transaction = new \ProcessingKz\Objects\Request\StartTransaction();
  $transaction->setTransaction($detailsEntity);

  try {
    $startResult = $client->startTransaction($transaction);
  }
  catch (Exception $e) {
    watchdog('processing_kz', 'Exception thrown while starting new transaction: @message (@code)', array(
      '@message' => $e->getMessage(),
      '@code' => $e->getCode(),
    ), WATCHDOG_ERROR);
    return FALSE;
  }

  if (!$startResult->getReturn()->getSuccess()) {
    watchdog('processing_kz', 'New transaction could not be started: @message', array(
      '@message' => $startResult->getReturn()->getErrorDescription(),
    ), WATCHDOG_ERROR);
    return FALSE;
  }

  $return = $startResult->getReturn();
  $reference = $return->getCustomerReference();

  $details->reference = $reference;
  $details->status = \ProcessingKz\Objects\Entity\StoredTransactionStatus::PENDING_CUSTOMER_INPUT;
  processing_kz_transaction_save($details);

  // Store redirect URL in an unmapped field, so it can passed by a caller to
  // the processing_kz_transaction_redirect().
  $details->redirect = $return->getRedirectUrl();

  return $details;
}

/**
 * Creates and configurates the API client instance.
 *
 * @return \ProcessingKz\Client
 */
function processing_kz_create_client() {
  if (variable_get('processing_kz_debug_mode')) {
    // Use default client settings to work with the test server.
    return new \ProcessingKz\Client();
  }
  else {
    $wsdl = variable_get('processing_kz_url_wsdl', 'https://payment.processinggmbh.ch/CNPMerchantWebServices/services/CNPMerchantWebService?wsdl');
    $location = variable_get('processing_kz_url_service', 'https://payment.processinggmbh.ch/CNPMerchantWebServices/services/CNPMerchantWebService');

    return new \ProcessingKz\Client($wsdl, array('location' => $location));
  }
}

/**
 * Updates and stores transaction status and auth code.
 *
 * @param object $details
 *   A record object from 'processing_kz_transactions' table.
 *
 * @return FALSE|string
 *   Updated transaction details object, or FALSE on failure.
 */
function processing_kz_sync_transaction_status($details) {
  if (empty($details) || empty($details->reference)) {
    return;
  }

  $client = processing_kz_create_client();
  $status = new \ProcessingKz\Objects\Request\GetTransactionStatus();
  $status
    ->setMerchantId(processing_kz_get_merchant_id())
    ->setReferenceNr($details->reference);

  if ($result = $client->getTransactionStatus($status)->getReturn()) {
    // Update auth code only if it is present in the response. If it is
    // missing, keep the old value intact.
    if ($result->getAuthCode()) {
      $details->auth_code = $result->getAuthCode();
    }

    $details->status = $result->getTransactionStatus();
    return processing_kz_transaction_save($details);
  }
  return FALSE;
}

/**
 * Checks that transaction has one of the two success statuses.
 *
 * @param object $details
 *   A record object from 'processing_kz_transactions' table.
 *
 * @return boolean
 *   Returns TRUE if transaction is authorised or paid;
 *   FALSE otherwise.
 */
function processing_kz_transaction_is_successful($details) {
  $success = array(
    \ProcessingKz\Objects\Entity\StoredTransactionStatus::AUTHORISED,
    \ProcessingKz\Objects\Entity\StoredTransactionStatus::PAID,
  );

  return !empty($details->status) && in_array($details->status, $success);
}

/**
 * Completes transaction by its reference.
 *
 * @param object $details
 *   A record object from 'processing_kz_transactions' table.
 *
 * @return NULL
 */
function processing_kz_complete_transaction($details) {
  return _processing_kz_complete_transaction($details, TRUE);
}

/**
 * Cancels transaction by its reference.
 *
 * @param object $details
 *   A record object from 'processing_kz_transactions' table.
 *
 * @return NULL
 */
function processing_kz_cancel_transaction($details) {
  return _processing_kz_complete_transaction($details, FALSE);
}

/**
 * Updates and syncs transaction status to complete it.
 */
function _processing_kz_complete_transaction($details, $result) {
  if (empty($details) || empty($details->reference)) {
    return;
  }

  if ($details->completed) {
    // Transaction is already either completed or canceled, no action is needed.
    return;
  }

  $details->completed = 1;
  $details = processing_kz_transaction_save($details);

  // Calling completeTransaction() is allowed only if the current status
  // is AUTHORISED. Check current status first.
  $details = processing_kz_sync_transaction_status($details);

  if ($details && $details->status == \ProcessingKz\Objects\Entity\StoredTransactionStatus::AUTHORISED) {
    $client = processing_kz_create_client();
    $complete = new \ProcessingKz\Objects\Request\CompleteTransaction();
    $complete
      ->setMerchantId(processing_kz_get_merchant_id())
      ->setReferenceNr($details->reference)
      ->setTransactionSuccess((boolean) $result);
    $completeResult = $client->completeTransaction($complete);

    // Synchronize updated status.
    processing_kz_sync_transaction_status($details);
  }
}

/**
 * Redirects user to the payment page.
 *
 * @param object $details
 *   Transaction details object, returned by the processing_kz_start_transaction()
 *   function with a 'redirect' property set.
 *
 * @return NULL
 */
function processing_kz_transaction_redirect($details) {
  if (!empty($details->redirect)) {
    $redirect = $details->redirect;
  }
  else {
    drupal_set_message(t('Transaction redirect URL is not specified.'), 'error');
    $redirect = url('<front>');
  }

  drupal_goto($redirect);
}
