<?php

/**
 * @file
 * Drupal menu page callbacks.
 */

/**
 * Menu page callback. Return page.
 *
 * @param object $transaction
 *   A record object from 'processing_kz_transactions' table.
 */
function processing_kz_return_page($transaction) {
  libraries_load('processing_kz');

  $status_class = '\ProcessingKz\Objects\Entity\StoredTransactionStatus';
  $non_terminal_statuses = array(
    $status_class::PENDING_CUSTOMER_INPUT,
    $status_class::PENDING_AUTH_RESULT,
    $status_class::AUTHORISED,
  );

  // Refresh transaction status if it is still in an intermediate state.
  if (empty($transaction->status) || in_array($transaction->status, $non_terminal_statuses)) {
    $transaction = processing_kz_sync_transaction_status($transaction);
  }

  // Allow modules to processe successful transaction if it is not yet processed and
  // payment was successful.
  if (!$transaction->processed && processing_kz_transaction_is_successful($transaction)) {
    $result = module_invoke_all('processing_kz_process', $transaction);
    if (in_array(TRUE, $result)) {
      // At least one module has processed transaction.
      $transaction->processed = 1;
      processing_kz_transaction_save($transaction);
    }
  }

  // Give modules a chance to display notification messages or redirect to the
  // next page in a workflow.
  $redirects = module_invoke_all('processing_kz_return', $transaction);
  $redirect = !empty($redirects) ? $redirects[0] : '<front>';
  drupal_goto($redirect);
}
